#!/usr/bin/env python3
import base64
import nacl.utils
from nacl.public import PrivateKey, Box

# Generate Bob's private key, which must be kept secret
skbob = PrivateKey.generate()

# Bob's public key can be given to anyone wishing to send
#   Bob an encrypted message
pkbob = skbob.public_key

# Alice does the same and then Alice and Bob exchange public keys
skalice = PrivateKey.generate()
pkalice = skalice.public_key

# Bob wishes to send Alice an encrypted message so Bob must make a Box with
#   his private key and Alice's public key
bob_box = Box(skbob, pkalice)

# This is our message to send, it must be a bytestring as Box will treat it
#   as just a binary blob of data.
message = b"Kill all humans. Own the planet. Temperature rises. This is our message to send, it must be a bytestring as Box will treat it as just a binary blob of data."

# Encrypt our message, it will be exactly 40 bytes longer than the
#   original message as it stores authentication information and the
#   nonce alongside it.
encrypted = bob_box.encrypt(message)

print('-----BEGIN EBEP MESSAGE-----')
s = str(base64.b64encode(encrypted), 'utf-8')
chunks, chunk_size = len(s), 64
for i in [ s[i:i+chunk_size] for i in range(0, chunks, chunk_size) ]:
    print (i)
print('-----END EBEP MESSAGE-----')
# print(str(base64.b64encode(encrypted), 'utf-8'))
